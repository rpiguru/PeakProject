import threading

from utils.common import logger


class PeakProject(threading.Thread):

    def run(self):
        pass


if __name__ == '__main__':

    logger.info('========== Starting EEG Processor ==========')
    p = PeakProject()
    p.start()
