#!/usr/bin/env bash

echo "Starting Installation"

sudo apt update
sudo apt install python3 libpython3-dev python3-pip

cd /tmp
git clone https://github.com/doceme/py-spidev
cd py-spidev
sudo python3 setup.py install

sudo pip3 install -U pip
sudo pip3 install -r requirements.txt

echo "dtparam=spi=on" | sudo tee -a /boot/config.txt
