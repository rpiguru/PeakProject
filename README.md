# EEG project based RPi 3 and ADS1299

## Components

- Raspberry Pi Model 3B+

- 3 ADS1299 chips

- 24 electrodes

## Connection Guide

TBD

## Installation

- Install the latest **Raspbian Stretch Lite** from [here](https://www.raspberrypi.org/downloads/raspbian/) and flash your micro sd card

- Clone this repository


        cd ~
        git clone https://gitlab.com/rpiguru/PeakProject

- Start installation


        cd PeakProject
        bash setup.sh

- And reboot!
        
        
        sudo reboot

